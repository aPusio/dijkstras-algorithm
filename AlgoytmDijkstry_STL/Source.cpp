#include <iostream>
#include <set>
#include <string>
#include <map>
#include <fstream>
#include <vector>
using namespace std;

const int nieskonczonosc = 2147483647;

struct wierzcholek {
	int skad;
	int index;
	int wartosc;
	wierzcholek(int index, int wartosc, int skad) {
		this->index = index;
		this->wartosc = wartosc;
		this->skad = skad;
	}
};

struct porownanie2 {
	bool operator()(const wierzcholek &a, const wierzcholek &b) {
		if (a.wartosc < b.wartosc) return true;
		else if (a.wartosc > b.wartosc) return false;
		else if (a.wartosc == b.wartosc)
			if (a.index < b.index) return true;
			else return false;
			return false;
	}
};


void wypiszQ(set<wierzcholek, porownanie2> &Q) {
	cout << "WYPISUJE KOLEJKE" << endl;
	for (set<wierzcholek, porownanie2>::iterator it = Q.begin(); it != Q.end(); it++)
	{
		cout << it->index << " ";
	}
	cout << endl;
	for (set<wierzcholek, porownanie2>::iterator it = Q.begin(); it != Q.end(); it++)
	{
		cout << it->wartosc << " ";
	}
	cout << endl;
}

int retOdleglosc(vector< vector< pair<int, int> > > &listaKrawedzi, int skad, int dokad)
{
	for (vector< pair<int, int> >::iterator it = listaKrawedzi[skad].begin(); it != listaKrawedzi[skad].end(); ++it)
	{
		if (it->first == dokad)
		{
			return it->second;
		}
	}
	return 0;
}

int dijikstra(vector< vector< pair<int, int> > > &listaKrawedzi, int liczbaMiast, int start, int stop)
{
	bool *S = new bool[liczbaMiast](); //init na false, wierzcholki zatwierdzone !
									   //bool *Q = new bool[liczbaMiast](); //init na false, wierzcholki odwiedzone ale nie zatwierdzone
	set<wierzcholek, porownanie2> Q;
	int *D = new int[liczbaMiast]; //najkrotsze sciezki od punkut do punktu
	int *P = new int[liczbaMiast](); //init 0, poprzednik miasta, 

	for (int i = 0; i < liczbaMiast; ++i)
		D[i] = nieskonczonosc;

	wierzcholek wybrany(start, 0, 0);
	Q.insert(wybrany);
	D[start] = 0;


	while (Q.size() != 0)
	{
		wybrany = *(Q.begin());
		Q.erase(Q.begin());

		int i = wybrany.index;
		if (i == stop) return D[i];
		S[i] = true;

		for (vector< pair<int, int> >::iterator it1 = listaKrawedzi[i].begin(); it1 != listaKrawedzi[i].end(); ++it1)
		{
			int j = it1->first;
			int krawedz = it1->second;
			if (S[j] != true)
			{
				if (D[i] + krawedz < D[j]) {
					D[j] = D[i] + krawedz;
					P[j] = i;
					Q.insert(wierzcholek(j, D[i] + krawedz, i));
				}
			}
		}
	}

	return D[stop];
}


int main() {
	ios_base::sync_with_stdio(0);
	int ilosc_testow, liczbaMiast, liczbaKrawedzi, skad, dokad, waga, iloscZapytan;
	string nazwaMiasta, skadStr, dokadStr;
	map <string, int> miasta;

	fstream myout;
	myout.open("myout.txt", ios::out);
	fstream myin;
	myin.open("input.txt", ios::in);

	cin >> ilosc_testow;

	for (int i = 0; i < ilosc_testow; ++i)
	{
		cin >> liczbaMiast;

		vector< vector< pair<int, int> > > listaKrawedzi;
		listaKrawedzi.resize(liczbaMiast);

		for (int j = 0; j < liczbaMiast; j++)
		{
			cin >> nazwaMiasta;

			miasta.insert(pair<string, int>(nazwaMiasta, j));
			cin >> liczbaKrawedzi;

			for (int k = 0; k < liczbaKrawedzi; ++k)
			{
				cin >> dokad;
				dokad--;
				cin >> waga;

				listaKrawedzi[j].push_back(pair<int, int>(dokad, waga));
			}
		}
		cin >> iloscZapytan;

		for (int j = 0; j < iloscZapytan; j++) {
			cin >> skadStr >> dokadStr;
			cout << dijikstra(listaKrawedzi, liczbaMiast, miasta[skadStr], miasta[dokadStr]);
			cout << "\n";
		}

		for (int k = 0; k < liczbaMiast; ++k)
			listaKrawedzi[k].clear();
	}

	//system("pause");
	return 0;
}